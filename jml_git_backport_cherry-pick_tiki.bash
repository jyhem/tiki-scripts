#!/bin/bash

if [ $# -eq 1 ]
then
	ORIGIN_BRANCH="$1"
else
	ORIGIN_BRANCH="master"
fi

echo "Updating branch from which you want to cherry-pick"
echo -n 'Perform "git checkout '${ORIGIN_BRANCH}'; git pull;"? (Y/n)'; read proceed
if [ "$proceed" = "n" ]
then
	git branch
	echo -n 'Choose another branch (hit Enter to exit): '; read ORIGIN_BRANCH
	if [ "$ORIGIN_BRANCH" = "" ]
	then
		echo "Exiting"
		exit 0
	fi
fi
git checkout ${ORIGIN_BRANCH}
git pull

git log --format=oneline -n 10
echo -n "Select commit you want to cherry-pick: "
read -r ORIGIN_COMMIT
ORIGIN_MESSAGE=$(git log $ORIGIN_COMMIT -n1 --format='%B')
echo "Previous message was: " $ORIGIN_MESSAGE

if [ "$ORIGIN_BRANCH" = "master" ]
then
	SVN_MIRROR="svn://svn.code.sf.net/p/tikiwiki/code/trunk"
else
	SVN_MIRROR="svn://svn.code.sf.net/p/tikiwiki/code/branches/${ORIGIN_BRANCH}"
fi
echo "$ svn log ${SVN_MIRROR} -l 5"
svn log ${SVN_MIRROR} -l 5
echo -n "Select SVN revision of previous commit: "
read ORIGIN_REV
if [ "$ORIGIN_REV" = "" ]
then
	echo "$ git reflog -n 10"
	git reflog -n 10
	echo -n "Select short git reference as substitute for SVN revision of previous commit: "
	read ORIGIN_REV
fi

git branch
echo -n "Select branch where you want to cherry-pick: "
read BRANCH

echo "=========="
echo "Recap:"
echo "ORIGIN_COMMIT: $ORIGIN_COMMIT"
echo "Message: $ORIGIN_MESSAGE"
echo "SVN revision: $ORIGIN_REV"
echo "Backport from ${ORIGIN_BRANCH} on branch: $BRANCH"
echo "=========="

echo "Confirm: \"git checkout $BRANCH\" (Y/n)"; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git checkout $BRANCH
echo 'Confirm: "git pull" (Y/n)'; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git pull

echo "Confirm: \"git cherry-pick -x $ORIGIN_COMMIT\" (Y/n)"; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git cherry-pick -x $ORIGIN_COMMIT
echo "Confirm: git commit --amend -m \"[bp/${ORIGIN_REV}]${ORIGIN_MESSAGE}\" (Y/n)"; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git commit --amend -m "[bp/${ORIGIN_REV}]${ORIGIN_MESSAGE}"

echo 'Confirm: "git push" (Y/n)'; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git push

echo "All done"
