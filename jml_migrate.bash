#!/bin/bash

usage()
{
	echo "Usage: $0 [-c] <path/to/local.php> <path/to/local.php> [path/to/tmp_directory]"
	echo "    -h : This message."
	echo "    -n : Do not ask any interactive question (crontab mode)."
	exit 0
}

while getopts ":hvcnD" optname
do
	case "$optname" in
		"h")
		usage;;
	"v")
		VERBOSE=1
		;;
	"n")
		export INTERACTIVE="false"
		;;
	"?")
		>&2 echo "Unknown option : -$OPTARG"
		usage;;
	":")
		usage;;
	*)
		>&2 echo "Unknown issue : -$OPTARG"
		usage;;
	esac
	shift $((OPTIND-1))
	OPTIND=1
done

if [[ -f "$1" ]]
then
	SOURCE_DB_DETECTED=$(grep "^[ 	]*\$dbs_tiki" "$1" | cut -d"'" -f2| tail -n 1);
	SOURCE_USER_DETECTED=$(grep "^[ 	]*\$user_tiki" "$1" | cut -d"'" -f2| tail -n 1);
	SOURCE_PASS_DETECTED=$(grep "^[ 	]*\$pass_tiki" "$1" | cut -d"'" -f2| tail -n 1);
	SOURCE_CHARSET_DETECTED=$(grep "^[ 	]*\$client_charset" "$1" | cut -d"'" -f2| tail -n 1);
else
	>&2 echo "ERROR: source local.php file not found"
	usage
	exit 1
fi
echo "== source $1 ($SOURCE_DB_DETECTED)"

if [[ -f "$2" ]]
then
	TARGET_DB_DETECTED=$(grep "^[ 	]*\$dbs_tiki" "$2" | cut -d"'" -f2| tail -n 1);
	TARGET_USER_DETECTED=$(grep "^[ 	]*\$user_tiki" "$2" | cut -d"'" -f2| tail -n 1);
	TARGET_PASS_DETECTED=$(grep "^[ 	]*\$pass_tiki" "$2" | cut -d"'" -f2| tail -n 1);
	TARGET_CHARSET_DETECTED=$(grep "^[ 	]*\$client_charset" "$2" | cut -d"'" -f2| tail -n 1);
else
	>&2 echo "ERROR: target local.php file not found"
	usage
	exit 1
fi
echo "== target $2 ($TARGET_DB_DETECTED)"

TEMPSTORAGEDIR="/tmp"
if [[ -d "$3" ]]
then
	$TEMPSTORAGEDIR=$3
	echo "→ $TEMPSTORAGEDIR"
else
	echo "→ $TEMPSTORAGEDIR (default value)"
fi

#if [[ "$COMPACT" == "true" ]]
#then
#	INSERTS="--extended-insert"
#	echo "Using ${INSERTS} (compact)"
#else
#	INSERTS="--skip-extended-insert"
#	echo "Using ${INSERTS}"
#fi

DATE=$(date +"%F_%T")
DUMP="$TEMPSTORAGEDIR/"dump_db_${SOURCE_DB_DETECTED}_${DATE}.sql
echo $DATE

echo -n "Proceed? (Y/n) "
read proceed
if [ "$proceed" = "n" ]
then
	exit 0
fi

touch $DUMP || exit 2
mysqldump -u "${SOURCE_USER_DETECTED}" --password="${SOURCE_PASS_DETECTED}" --default-character-set="${SOURCE_CHARSET_DETECTED}" -Qqf --extended-insert "${SOURCE_DB_DETECTED}" > $DUMP
echo "temporary dump: " $DUMP

mysql --force -u "${TARGET_USER_DETECTED}" --password="${TARGET_PASS_DETECTED}" --default-character-set="${TARGET_CHARSET_DETECTED}" "${TARGET_DB_DETECTED}" < "$DUMP"

echo "Migration done"

rm "$DUMP" && echo "Temporary dump removed" || >&2 echo "ERROR: temporary dump cound not be removed: " "$DUMP"


