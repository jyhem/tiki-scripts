#!/bin/bash                                                                                                                                                                                                                                                                    

usage()
{
  echo "Usage: $0 path/to/local.php path/to/local.php"
  echo "    -h : This message"
  echo "    -v : Verbose"
}

while getopts ":hv" optname
do
  case "$optname" in
    "h")
    usage;;
  "v")
    VERBOSE=1
    ;;
  "?")
    echo "Argument inconnu : -$OPTARG"
    usage;;
  ":")
    usage;;
  *)
    echo "Problème inconnu : -$OPTARG"
    usage;;
  esac
  shift $((OPTIND-1))
  OPTIND=1
done

echo " < $1"
if [[ -f "$1" ]]
then
  DB_DETECTED_1=$(grep "^[ 	]*\$dbs_tiki" "$1" | cut -d"'" -f2| tail -n 1);
  USER_DETECTED_1=$(grep "^[ 	]*\$user_tiki" "$1" | cut -d"'" -f2| tail -n 1);
  PASS_DETECTED_1=$(grep "^[ 	]*\$pass_tiki" "$1" | cut -d"'" -f2| tail -n 1);
  CHARSET_DETECTED_1=$(grep "^[ 	]*\$client_charset" "$1" | cut -d"'" -f2| tail -n 1);
else
  echo "ERROR: First local.php file not found"
  usage
  exit 1
fi

echo " > $2"
if [[ -f "$2" ]]
then
  DB_DETECTED_2=$(grep "^[ 	]*\$dbs_tiki" "$2" | cut -d"'" -f2| tail -n 1);
  USER_DETECTED_2=$(grep "^[ 	]*\$user_tiki" "$2" | cut -d"'" -f2| tail -n 1);
  PASS_DETECTED_2=$(grep "^[ 	]*\$pass_tiki" "$2" | cut -d"'" -f2| tail -n 1);
  CHARSET_DETECTED_2=$(grep "^[ 	]*\$client_charset" "$2" | cut -d"'" -f2| tail -n 1);
else
  echo "ERROR: Second local.php file not found"
  usage
  exit 1
fi

tmpfile1=$(mktemp /tmp/tmp1_$(date +"%Y-%m-%d_%T_XXXXXX"))  # Create a temporal file in the default temporal folder of the system
# Lets do some magic for the tmpfile to be removed when this script ends, even if it crashes
exec {FD_W_1}>"$tmpfile1"  # Create file descriptor for writing, using first number available
exec {FD_R_1}<"$tmpfile1"  # Create file descriptor for reading, using first number available
echo "tmpfile1 : $tmpfile1"
rm "$tmpfile1"  # Delete the file, but file descriptors keep available for this script

tmpfile2=$(mktemp /tmp/tmp2_$(date +"%Y-%m-%d_%T_XXXXXX"))  # Create a temporal file in the default temporal folder of the system
# Lets do some magic for the tmpfile to be removed when this script ends, even if it crashes
exec {FD_W_2}>"$tmpfile2"  # Create file descriptor for writing, using first number available
exec {FD_R_2}<"$tmpfile2"  # Create file descriptor for reading, using first number available
echo "tmpfile2 : $tmpfile2"
rm "$tmpfile2"  # Delete the file, but file descriptors keep available for this script

trap 'rm -f -- "$tmpfile2"; rm -f -- "$tmpfile1"' INT TERM HUP EXIT

#mysqldump -u "${USER_DETECTED_1}" --password="${PASS_DETECTED_1}" --default-character-set="${CHARSET_DETECTED_1}" -Qqf --skip-extended-insert "${DB_DETECTED_1}" "tiki_preferences" >&$FD_W_1
#mysqldump -u "${USER_DETECTED_2}" --password="${PASS_DETECTED_2}" --default-character-set="${CHARSET_DETECTED_2}" -Qqf --skip-extended-insert "${DB_DETECTED_2}" "tiki_preferences" >&$FD_W_2
mysqldump -u "${USER_DETECTED_1}" --password="${PASS_DETECTED_1}" --default-character-set="${CHARSET_DETECTED_1}" -Qqf --skip-extended-insert "${DB_DETECTED_1}" "tiki_preferences" > "$tmpfile1"
mysqldump -u "${USER_DETECTED_2}" --password="${PASS_DETECTED_2}" --default-character-set="${CHARSET_DETECTED_2}" -Qqf --skip-extended-insert "${DB_DETECTED_2}" "tiki_preferences" > "$tmpfile2"

diff "$tmpfile1" "$tmpfile2"

exit 0
