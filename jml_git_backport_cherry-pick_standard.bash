#!/bin/bash

if [ $# -ge 1 ]
then
	ORIGIN_BRANCH="$1"
else
	ORIGIN_BRANCH="dev"
fi
echo "Updating from branch '${ORIGIN_BRANCH}'"

if [ $# -ge 2 ]
then
	BRANCH="$2"
else
	BRANCH="prod"
fi
echo "Updating to branch '${BRANCH}'"

echo "Updating branch from which you want to cherry-pick"
echo -n 'Perform "git checkout '${ORIGIN_BRANCH}'; git pull;"? (Y/n) '; read proceed
if [ "$proceed" = "n" ]
then
	git branch
	echo -n 'Choose another branch (hit Enter to exit): '; read ORIGIN_BRANCH
	if [ "$ORIGIN_BRANCH" = "" ]
	then
		echo "Exiting"
		exit 0
	fi
fi
git checkout ${ORIGIN_BRANCH}
git pull

git log --format=oneline -n 10
echo -n "Select commit you want to cherry-pick: "
read -r ORIGIN_COMMIT
ORIGIN_MESSAGE=$(git log $ORIGIN_COMMIT -n1 --format='%B')
echo "Previous message was: " $ORIGIN_MESSAGE

git branch
echo -n "Select branch where you want to cherry-pick: [${BRANCH}] "
read NEWBRANCH
if [ "$NEWBRANCH" != "" ]
then
	BRANCH="${NEWBRANCH}"
fi

echo "=========="
echo "Recap:"
echo "ORIGIN_COMMIT: $ORIGIN_COMMIT"
echo "Message: $ORIGIN_MESSAGE"
echo "Backport from ${ORIGIN_BRANCH} on branch: $BRANCH"
echo "=========="

echo "Confirm: \"git checkout $BRANCH\" (Y/n) "; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git checkout $BRANCH
echo 'Confirm: "git pull" (Y/n) '; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git pull

echo "Confirm: \"git cherry-pick -x $ORIGIN_COMMIT\" (Y/n) "; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git cherry-pick -x $ORIGIN_COMMIT

echo 'Confirm: "git push" (Y/n) '; read proceed
if [ "$proceed" = "n" ]; then echo "Exiting"; exit 0; fi
git push

echo "All done"
