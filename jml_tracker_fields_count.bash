#!/bin/bash

usage()
{
	echo "Usage: $0 [path/to/local.php]"
}

if [[ "$1" = "" ]]
then
	# If no parameter was given, try to guess
	if [[ -f db/local.php ]]
	then
		LOCALFILE="db/local.php"
		echo "Detected: $LOCALFILE"
	else
		usage
		exit 1
	fi
else
	# Use the one provided
	LOCALFILE="$1"
fi

echo -n "Using: $LOCALFILE"
if [[ -f "$LOCALFILE" ]]
then
  DB_DETECTED=$(grep "^[ 	]*\$dbs_tiki" "$LOCALFILE" | cut -d"'" -f2| tail -n 1);
  USER_DETECTED=$(grep "^[ 	]*\$user_tiki" "$LOCALFILE" | cut -d"'" -f2| tail -n 1);
  PASS_DETECTED=$(grep "^[ 	]*\$pass_tiki" "$LOCALFILE" | cut -d"'" -f2| tail -n 1);
else
  echo "ERROR: local.php file not found"
	usage
  exit 1
fi
echo " with database $DB_DETECTED"

LIST=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT * FROM tiki_tracker_fields;" )
COUNT=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT COUNT(*) FROM tiki_tracker_fields;")
echo "Number of fields: $COUNT"
#echo "LIST of fields: $LIST"

