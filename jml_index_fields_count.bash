#!/bin/bash

usage()
{
	echo "Usage: $0 [path/to/local.php]"
}

if [[ "$1" = "" ]]
then
	# If no parameter was given, try to guess
	if [[ -f db/local.php ]]
	then
		LOCALFILE="db/local.php"
		echo "Detected: $LOCALFILE"
	else
		usage
		exit 1
	fi
else
	# Use the one provided
	LOCALFILE="$1"
fi

echo -n "Using: $LOCALFILE"
if [[ -f "$LOCALFILE" ]]
then
  DB_DETECTED=$(grep "^[ 	]*\$dbs_tiki" "$LOCALFILE" | cut -d"'" -f2| tail -n 1);
  USER_DETECTED=$(grep "^[ 	]*\$user_tiki" "$LOCALFILE" | cut -d"'" -f2| tail -n 1);
  PASS_DETECTED=$(grep "^[ 	]*\$pass_tiki" "$LOCALFILE" | cut -d"'" -f2| tail -n 1);
else
  echo "ERROR: local.php file not found"
	usage
  exit 1
fi
echo " with database $DB_DETECTED"

#mysql --force -u "${USER_DETECTED}" --password="${PASS_DETECTED}" --default-character-set=utf8 "${DB_DETECTED}" < "$IMPORT"
#mysql -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SHOW TABLES;"
#mysql -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT * FROM information_schema.tables WHERE TABLE_SCHEMA='${DB_DETECTED}' AND table_name like 'index_%';";
INDEXES=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SHOW TABLES WHERE Tables_in_${DB_DETECTED} LIKE 'index_%';" | grep -v index_pref_ )
NB_INDEXES=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SHOW TABLES WHERE Tables_in_${DB_DETECTED} LIKE 'index_%';"| grep -v index_pref_ | wc -l)
SEARCHENGINE=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT value FROM tiki_preferences WHERE name = 'unified_engine';" )
echo "Search engine: $SEARCHENGINE"
if [[ "$SEARCHENGINE" = "mysql" || "$SEARCHENGINE" = "" ]]
then
	CURRENTINDEX=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT value FROM tiki_preferences WHERE name = 'unified_mysql_index_current';" )
	echo "Current index: $CURRENTINDEX"
fi
echo "---"
echo "$INDEXES"
if [[ "$NB_INDEXES" = "0" ]]
then
	echo "No index found. Nothing to do."
	exit 0
fi
echo "${NB_INDEXES} indexes found."

for i in $INDEXES
do
	echo
	echo "---"
	if [ "$i" = "$CURRENTINDEX" ]
	then
		echo -n "Current "
	fi
	echo "Index: $i"
	LIST=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$i' AND table_schema = '$DB_DETECTED';")
	COUNT=$(mysql --skip-column-names --silent -u "${USER_DETECTED}" --password="${PASS_DETECTED}" "${DB_DETECTED}" -e "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$i' AND table_schema = '$DB_DETECTED';"| wc -l)
	echo "Columns count: $COUNT"
	#echo "Columns list: $LIST"
done

